from ctypes import sizeof
from pickle import GLOBAL
from turtle import speed
import datetime
import datetime as dt
from math import radians, cos, sin, asin, sqrt
import gpxpy
import math
import gpxpy.gpx
import json
import time, threading
import os.path
import math
from geopy.distance import lonlat, distance
import geopy.distance
from geopy.distance import Distance



pedenstrianPoints = []   
TestArray = []      # objects with zero avg speed

MainRoutesArr = []          

class GPSPoint:
    def __init__(self):
        self.latitude = 0
        self.longitude = 0
        self.speed = 0
        self.speedX = 0
        self.speedY = 0
        self.speedZ = 0
        self.altitude = 0
        self.unixTime = 0
        self.time = "0"
    
    def find_speed(self,previous_point):
        p1 = (self.longitude, self.latitude)
        p2 = (previous_point.longitude, previous_point.latitude)
        flat_distance = distance(p1[:2], p2[:2]).meters
        time = (self.unixTime - previous_point.unixTime)
        if time > 0:
            self.speed = flat_distance / time
        else :
            self.speed = flat_distance / 1
        if self.speed<0.0009:
            self.speed = 0.0
        
    


class RouteC:
    def __init__(self, name):
        self.RouteArr = []
        self.name = name


    #function that recognises fall 
    def CheckForFall(self):
        for i in range(len(self.RouteArr)):
            if(i>1): 
                if self.RouteArr[i-1].altitude - self.RouteArr[i].altitude > 0.2:                     #diff in elevetion -> fall event
                    print("fell at: " + str(self.RouteArr[i].time))
                else:
                    print("No fall recorded")


    def addpoint_to_Route_array(self,point):
        self.RouteArr.append(point)


    #function that returns the index of the position in each class.array that locates pause 
    def PauseCheckIndex(self):

        pause_index=0
        for i in range(len(self.RouteArr)):
            if(i>3): 
                if self.RouteArr[i].speed==0 and self.RouteArr[i-1].speed==0 and self.RouteArr[i-2].speed==0 and self.RouteArr[i-3].speed==0:
                    pause_index = i
                    break
        return pause_index
                

    #function to find pause time for each route
    def PauseTime(self):
        
        Pause_Time = 0
        index = self.PauseCheckIndex()
        #print(index)
        if index ==0:
            return 0
        else:
            for i in range(index,len(self.RouteArr)):
                if self.RouteArr[i].speed==0:
                    Pause_Time = Pause_Time + 1
            return Pause_Time


    #function for distance covered in each route
    def distance(self):
        if len(self.RouteArr)>1:

            lon1 = radians(self.RouteArr[0].longitude)
            lon2 = radians(self.RouteArr[len(self.RouteArr)-1].longitude)

            lat1 = radians(self.RouteArr[0].latitude)
            lat2 = radians(self.RouteArr[len(self.RouteArr)-1].latitude)

            # Haversine formula
            dlon = lon2 - lon1
            dlat = lat2 - lat1
            a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
        
            c = 2 * asin(sqrt(a))
            
            # Radius of earth in kilometers
            r = 6371
            
            # calculate the result
            distance = c * r
            #print("The covered distance for " + self.name + " is: " + str(distance) + " in kilometers")
        else:
            distance =0
        return distance

    def find_speed(self):
        p1 = (self.RouteArr[len(self.RouteArr-1)].longitude, self.RouteArr[len(self.RouteArr-1)].latitude)
        p2 = (self.RouteArr[len(self.RouteArr-2)].longitude, self.RouteArr[len(self.RouteArr-2)].latitude)
        flat_distance = distance.distance(p1[:2], p2[:2]).km
        self.RouteArr[len(self.RouteArr-1)].speed = flat_distance / (self.RouteArr[len(self.RouteArr-2)].time - self.RouteArr[len(self.RouteArr-2)].time)

    def Route_walking_time(self):
        total_walking_time=0
        if len(self.RouteArr)<1:
            return 0
        else:
            for i in range(len(self.RouteArr)):
                if self.RouteArr[i].speed!=0:
                    total_walking_time = total_walking_time + 1
            return total_walking_time

    def write_stats_to_file(self,filename):         #gia kathe route
        dictionary ={
        "avgPause" : self.PauseTime(),
        "totwalkingtime" : self.Route_walking_time(),
        "avgDistance" : self.distance(),
        "routeNum" : 1
        }
    
        with open(filename, 'w') as outfile:
            json.dump(dictionary, outfile)
    

def find_average_total_distance(MainRoutesArr):
    dist = 0
    average_dist=0
    for i in range(len(MainRoutesArr)):
        for j in range(len(MainRoutesArr[i].RouteArr)):
            dist += MainRoutesArr[i].distance()

    average_dist = dist/len(MainRoutesArr)
    return average_dist
            

def find_average_walking_time(MainRoutesArr):
    avg_walk_time = 0
    for i in range(len(MainRoutesArr)):
        avg_walk_time = MainRoutesArr[i].Route_walking_time()/len(MainRoutesArr)
    return avg_walk_time

def find_average_pause_time(MainRoutesArr):
    avg_pause_time = 0
    for i in range(len(MainRoutesArr)):
        avg_pause_time = MainRoutesArr[i].PauseTime()/len(MainRoutesArr)
    return avg_pause_time




def write_average_results_file(input):

       
    dictionary ={
        "avgPause" : find_average_pause_time(input),
        "totwalkingtime" : find_average_walking_time(input),
        "avgDistance" : find_average_total_distance(input),
        "routeNum" : len(input)
        }
    
    if os.path.exists('/opt/prolong/') == True:
        with open('/opt/prolong/gait-analysis-result.json', 'w') as outfile:
            json.dump(dictionary, outfile)
    else:    
        with open('C:/Projects/PROLONG/CODE/gait-analysis-result.json', 'w') as outfile:
            json.dump(dictionary, outfile)

    



#main#####################################################################################################################################################

def read_and_parse_file():

    

    global pedenstrianPoints, MainRoutesArr
    try:
        if os.path.exists('/opt/prolong/current-location-coordinates.json') == True:
            with open('/opt/prolong/current-location-coordinates.json') as f:
                json_file = json.load(f)
        elif os.path.exists('C:/Projects/PROLONG/CODE/current-location-coordinates.json'):    
            with open('C:/Projects/PROLONG/CODE/current-location-coordinates.json') as f:
                json_file = json.load(f)
        else:
            threading.Timer(2,read_and_parse_file).start()
            return
    except Exception as ex:
        print (ex)
        threading.Timer(2,read_and_parse_file).start()
        return
        
    print(json_file)
    if 'unixTime' in json_file:
        if len(pedenstrianPoints)<1 or json_file['unixTime']!=pedenstrianPoints[len(pedenstrianPoints)-1].unixTime:             #ta unixtime δεν πρεπει να ειναι ισα να αλλαξει οταν θα δεχεσαι σωστα αρχεια
            GPSP = GPSPoint()
            GPSP.latitude = json_file['latitude']
            GPSP.longitude = json_file['longitude']
            GPSP.speed = json_file['speed']
            GPSP.altitude = json_file['altitude']
            GPSP.unixTime = json_file['unixTime']
            untime = json_file['unixTime']/1000
            date_time = datetime.datetime.fromtimestamp(untime)  
            GPSP.time = date_time
            if len(pedenstrianPoints)>0:
                GPSP.find_speed(pedenstrianPoints[len(pedenstrianPoints)-1])
            pedenstrianPoints.append(GPSP)
            
                #GPSP.find_speed()
        
    else:
        for x in json_file:
            #print(x['latitude'], x['longitude'], x['speed'], x['altitude'])
        #   if x['unixTime'] < x-1['unixTime']:
            if len(pedenstrianPoints)<1 or x['unixTime']>pedenstrianPoints[len(pedenstrianPoints)-1].unixTime:
                GPSP = GPSPoint()
                GPSP.latitude = x['latitude']
                GPSP.longitude = x['longitude']
                GPSP.speed = x['speed']
                GPSP.altitude = x['altitude']
                GPSP.unixTime = x['unixTime']
                untime = x['unixTime']/1000
                date_time = datetime.datetime.fromtimestamp(untime)  
                GPSP.time = date_time
                if len(pedenstrianPoints)>0:
                    GPSP.find_speed(pedenstrianPoints[len(pedenstrianPoints)-1])
                pedenstrianPoints.append(GPSP)
            
            else:
                continue

    print(pedenstrianPoints[len(pedenstrianPoints)-1].speed)

    name_counter = 2
    stop_counter = 0
    j=0

    if len(pedenstrianPoints)<4:
        threading.Timer(2,read_and_parse_file).start()
        
        return

    #manually create the first route class named route1 and append it on mainroutes array list that contains classes
    
    if len(MainRoutesArr)==0:
        MainRoutesArr.append(RouteC("Route1"))
    if len(pedenstrianPoints)>3:
        if(pedenstrianPoints[len(pedenstrianPoints)-1].speed!=0) and pedenstrianPoints[len(pedenstrianPoints)-2].speed==0 and pedenstrianPoints[len(pedenstrianPoints)-3].speed==0 and pedenstrianPoints[len(pedenstrianPoints)-4].speed==0:
            #print(pedenstrianPoints[len(pedenstrianPoints)-1].speed, pedenstrianPoints[len(pedenstrianPoints)-1].speed, pedenstrianPoints[i-3].speed)
            filename_to_write = "route_statistics_"+str(int(time.time()))+".json"
            if os.path.exists('/opt/prolong/') == True:
                filename_to_write = '/opt/prolong/'+filename_to_write
            else:    
                filename_to_write = 'C:/Projects/PROLONG/CODE/' + filename_to_write

            MainRoutesArr[j].write_stats_to_file(filename_to_write)
            
            
            

            j = j + 1
            MainRoutesArr.append(RouteC("Route" + str(name_counter)))
            name_counter = name_counter + 1
            MainRoutesArr[j].addpoint_to_Route_array(pedenstrianPoints[len(pedenstrianPoints)-1])
            
            
            pedenstrianPoints.clear()
            stop_counter = 0
        else:                        
            MainRoutesArr[j].addpoint_to_Route_array(pedenstrianPoints[len(pedenstrianPoints)-1])





    for i in range(len(MainRoutesArr)):
        for j in range(len(MainRoutesArr[i].RouteArr)):
            if j>0:
                lat2 = MainRoutesArr[i].RouteArr[j].latitude
                lat1 = MainRoutesArr[i].RouteArr[j-1].latitude
                long2 = MainRoutesArr[i].RouteArr[j].longitude
                long1 = MainRoutesArr[i].RouteArr[j-1].longitude
                dy = lat2 - lat1
                dx = math.cos(math.pi/180*lat1)*(long2 - long1)
                angle = math.atan2(dy, dx)
                #print("the angle is " + str(angle) + " in degrees")


    
    if len(MainRoutesArr)>1:
        write_average_results_file(MainRoutesArr)

    threading.Timer(2,read_and_parse_file).start()
    

    


def main():
    
    read_and_parse_file()
    


main()